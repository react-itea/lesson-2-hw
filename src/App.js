import React, { Component } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'
import UserItem from './components/UserItem'
import LoaderImg from './components/LoaderImg'
import Table from './components/Table'
import Row from './components/Row'
import Cell from './components/Cell'

class App extends Component {

  state = {
    users: []
  }

  componentDidMount = async () => {

    const response = await fetch('http://www.json-generator.com/api/json/get/cdZfJCjIia?indent=2')
    const users = await response.json()
    this.setState({
      users
    })
  }

  actionBtn = (id) => _ => {
    const newUsers = this.state.users.map(user => {
      if (id === user._id) {
        user.interviewed = !user.interviewed
      }
      return user
    })
    this.setState({
      users: newUsers
    })
  }

  render = () => {

    const { actionBtn } = this
    const { users } = this.state

    return (
      <div className="container">
        <Table>
          <Row head="true">
            <Cell type="" background="red">#</Cell>
            <Cell type="date">2</Cell>
            <Cell type="number">3</Cell>
            <Cell type="money" currency="$">4</Cell>
          </Row>
          <Row>
            <Cell type="" background="red">1</Cell>
            <Cell type="date">2</Cell>
            <Cell type="number">3</Cell>
            <Cell type="money" currency="$">4</Cell>
          </Row>
        </Table>

        <LoaderImg></LoaderImg>
        <ul
          className="list-group"
        >
          {
            // console.log(users)
            users.map(user => {
              return (
                <UserItem
                  key={user._id}
                  id={user._id}
                  interviewed={user.interviewed}
                  user={user.user}
                  action={actionBtn}
                />
              )
            })
          }
        </ul>
      </div>
    )
  }
}

export default App;
