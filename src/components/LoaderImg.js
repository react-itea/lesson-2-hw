import React, { Component } from 'react'
import { Spinner } from 'react-bootstrap'
import ph from '../images/ph.svg'

class LoaderImg extends Component {

  state = {
    loadStatus: false,
    errorStatus: false
  }

  handleImgLoad = () => {
    this.setState({ loadStatus: true })
  }

  handleImgError = () => {
    this.setState({loadStatus: true, errorStatus: true})
  }

  render = () => {

    const { handleImgLoad, handleImgError } = this
    const { loadStatus, errorStatus } = this.state
    const url = "https://media.springernature.com/lw630/nature-cms/uploads/cms/pages/2913/top_item_image/cuttlefish-e8a66fd9700cda20a859da17e7ec5748.png"

    return (
      <>
        {!loadStatus && <Spinner animation="border" />}
        <img
          style={{ display: loadStatus ? 'block' : 'none' }}
          src={url}
          onLoad={handleImgLoad}
          onError={handleImgError}
          alt=""
        />
        {
          errorStatus && <img src={ph} width="128" height="128" alt="" />
        }
      </>
    )
  }
}

export default LoaderImg
