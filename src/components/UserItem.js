import React from 'react'
import { Button } from './Button'

const UserItem = ({interviewed, id, user, action}) => {

  return (
    <>
      <li
        className={interviewed ? 'list-group-item list-group-item-success' : 'list-group-item list-group-item-danger'}
      >
        {user.name}
      </li>
      <Button
        id={id}
        action={action}
      >
        Change status
      </Button>
      <hr></hr>
    </>
  )
}

export default UserItem