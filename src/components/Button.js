import React from 'react'

export const Button = ({ action, id, children }) => {


  return (
    <button
      className="btn btn-primary"
      onClick={action(id)}
    >
      {children}
    </button>
  )
}