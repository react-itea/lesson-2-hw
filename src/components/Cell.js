import React from 'react'

const Cell = ({ type, currency, cells, background, color, children }) => {

  let cellClass = ''

  switch (type) {
    case 'text':
      cellClass = 'default-cell'
      break;

    case 'date':
      cellClass = 'italic-cell'
      break;

    case 'number':
      cellClass = 'right-cell'
      break;

    case 'money':
      cellClass = 'right-cell'
      break;

    default:
      break;
  }

  return (
    <td
      className={cellClass}
      bgcolor={background}
      colSpan={cells}
      style={{color: color}}
    >
      {type === 'money' ? currency : ''}{children}
      
    </td>
  )
}

Cell.defaultProps = {
  type: 'text',
  cells: 1,
  background: 'transparent',
  color: 'black',
  currency: '$'
}

export default Cell