import React from 'react'

const Row = ({head, children}) => {

  return (
      head
        ? <tr className="bold-children">{children}</tr>
        : <tr>{children}</tr>
  )
}

Row.defaultProps = {
  head: false
}

export default Row